import json
import os
import logging
from datetime import datetime
from datetime import timedelta

# from _mysql_exceptions import IntegrityError
from dateutil.relativedelta import relativedelta
from flask import Flask, request, flash, url_for, redirect, \
    render_template, send_from_directory, g, Response, session, current_app
from flask.json import jsonify
from flask_login import logout_user, LoginManager, current_user, login_user, login_required
from flask_sqlalchemy import SQLAlchemy
# from gcloud.exceptions import NotFound
from sqlalchemy import or_, desc
from werkzeug.security import generate_password_hash, check_password_hash
# from werkzeug.utils import secure_filename
# from gcloud import storage
from flask_cachecontrol import (
    FlaskCacheControl,
    cache,
    cache_for,
    dont_cache)

ENV_VARS = ['OPENSHIFT_MYSQL_DB_URL',
            'OPENSHIFT_APP_NAME'
            'CLINIC_MANAGER_USERNAME',
            'CLINIC_MANAGER_PASSWORD',
            'CLINIC_MANAGER_EMAIL',
            'GOOGLE_APPLICATION_CREDENTIALS']
ALLOWED_EXTENSIONS = {'txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif'}
UPLOAD_FOLDER = '/tmp/uploads'

application = Flask(__name__)
application.config['SQLALCHEMY_DATABASE_URI'] = 'mysql+pymysql://%s:%s@%s:%s/clinicmanager' \
        % (os.environ.get('MYSQL_USER'), os.environ.get('MYSQL_PASSWORD'),
           os.environ.get('MYSQL_SERVICE_HOST'), os.environ.get('MYSQL_SERVICE_PORT'))
application.config['SECRET_KEY'] = 'super secret key'
db = SQLAlchemy(application)
db.create_all()

login_manager = LoginManager()
login_manager.init_app(application)
login_manager.login_view = 'login'

flask_cache_control = FlaskCacheControl()
flask_cache_control.init_app(application)

# Set the env variable to the Google Service Key Path
# Command: rhc env set GOOGLE_APPLICATION_CREDENTIALS=clinic-manager-a3347082852d.json -a clinic
# http://gcloud-python.readthedocs.org/en/latest/gcloud-auth.html
# gcloud_client = storage.Client(project='clinic-manager')

application.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
FLASH_CLEAR_ENDPOINTS = ['register', 'patient', 'deletePatient',
                         'newAppointment', 'treatment', 'deleteTreatment',
                         'prescriptionUpload']
gunicorn_error_handlers = logging.getLogger('gunicorn.error').handlers
application.logger.handlers.extend(gunicorn_error_handlers)

db.session.query("1").from_statement("SELECT 1").all()
application.logger.info("db working")
CLINIC_BUCKET_NAME = 'clinic-manager'


class User(db.Model):
    """
    This table is used to store login as well as basic information about the
    user.
    """
    __tablename__ = "users"
    id = db.Column('user_id', db.Integer, primary_key=True)
    username = db.Column('username', db.String(20), unique=True, index=True)
    password = db.Column('password', db.String(250))
    email = db.Column('email', db.String(50), unique=True, index=True)
    registered_on = db.Column('registered_on', db.DateTime)

    def __init__(self, username, password, email):
        self.username = username
        self.set_password(password)
        self.email = email
        self.registered_on = datetime.utcnow()

    def set_password(self, password):
        self.password = generate_password_hash(password)

    def check_password(self, password):
        return check_password_hash(self.password, password)

    def is_authenticated(self):
        return True

    def is_active(self):
        return True

    def is_anonymous(self):
        return False

    def get_id(self):
        return unicode(self.id)

    def __repr__(self):
        return '<User %r>' % self.username


class Patient(db.Model):
    """
    This table is used to store information about the patients.
    """
    __tablename__ = "patient"
    id = db.Column('id', db.Integer, primary_key=True)
    name = db.Column('name', db.String(20), index=True)
    patient_number = db.Column('patient_number', db.String(20), unique=True, index=True)
    mobile_number = db.Column('mobile_number', db.String(250), index=True)
    contact_number = db.Column('contact_number', db.String(50), index=True)
    email_address = db.Column('email_address', db.String(50), index=True)
    gender = db.Column('gender', db.String(50))
    address = db.Column('address', db.String(50))
    city = db.Column('city', db.String(50))
    country = db.Column('country', db.String(50))
    pincode = db.Column('pincode', db.String(50))
    dob = db.Column('dob', db.DateTime)
    blood_group = db.Column('blood_group', db.String(50))
    appointments = db.relationship('Appointment', backref='appointment',
                                   lazy='dynamic')

    def __init__(self, name, patient_number, mobile_number, contact_number, email_address,
                 gender, address, city, country, pincode, dob, blood_group):
        assert patient_number is not None
        self.name = name
        self.patient_number = patient_number
        self.mobile_number = mobile_number
        self.contact_number = contact_number
        self.email_address = email_address
        self.gender = gender
        self.address = address
        self.city = city
        self.country = country
        self.pincode = pincode
        self.dob = dob
        self.blood_group = blood_group

    def __repr__(self):
        return '<Patient %r>' % self.name


class Treatment(db.Model):
    """
    This table is used to store information about different treatments.
    """
    __tablename__ = "treatment"
    id = db.Column('id', db.Integer, primary_key=True)
    name = db.Column('name', db.String(20), unique=True, index=True)
    cost = db.Column('cost', db.Float)
    notes = db.Column('notes', db.String(250))
    appointments = db.relationship('Appointment', backref='appointmentTreatments',
                                   lazy='dynamic')

    def __init__(self, name, cost, notes):
        self.name = name
        self.cost = cost
        self.notes = notes

    def __repr__(self):
        return '<Treatment %r>' % self.name


class Appointment(db.Model):
    """
    This table is used to store information about patient appointments
    """
    __tablename__ = "appointment"
    id = db.Column('id', db.Integer, primary_key=True)
    patient_id = db.Column(db.Integer, db.ForeignKey('patient.id'))
    appt_date = db.Column('cost', db.DateTime)
    treatment = db.Column(db.Integer, db.ForeignKey('treatment.id'))
    prescription = db.Column(db.Integer, db.ForeignKey('prescription.id'))

    def __init__(self, pid, appt_date, tid):
        self.patient_id = pid
        self.appt_date = appt_date
        self.treatment = tid

    def __repr__(self):
        return '<Appointment %r>' % self.patient_id, self.appt_date


class Prescription(db.Model):
    """
    This table is used to store the prescriptions for patients
    """
    __tablename__ = "prescription"
    id = db.Column('id', db.Integer, primary_key=True)
    patient_id = db.Column(db.Integer, db.ForeignKey('patient.id'))
    doc_url = db.Column('doc_url', db.String(250))

    def __init__(self, pid, doc_url):
        self.patient_id = pid
        self.doc_url = doc_url


# Registration is limited to only the specific manager user
@application.route('/register', methods=['GET', 'POST'])
def register():
    if request.method == 'GET':
        return render_template('reg.html')
    user = User(request.form['username'], request.form['password'], request.form['email'])
    db.session.add(user)
    db.session.commit()
    flash('User successfully registered', 'success')
    return redirect(url_for('login'))


def default_admin_user(username, password):
    if username == os.environ.get('CLINIC_MANAGER_USERNAME') and password == os.environ.get('CLINIC_MANAGER_PASSWORD'):
        return True
    return False


@application.route('/login', methods=['GET', 'POST'])
def login():
    application.logger.info('MySql: %s' % os.environ.get('SQLALCHEMY_DATABASE_URI'))
    application.logger.info('Users [%s]' % User.query.all())

    if request.method == 'GET':
        return render_template('login.html')

    username = request.form['username']
    password = request.form['password']
    remember_me = False
    if 'remember_me' in request.form:
        remember_me = True

    registered_user = User.query.filter_by(username=username).first()

    if registered_user is None and default_admin_user(username, password):
        email = os.environ.get('CLINIC_MANAGER_EMAIL')
        user = User(request.form['username'], request.form['password'], email)
        db.session.add(user)
        db.session.commit()
        registered_user = User.query.filter_by(username=username).first()
    elif registered_user is None:
        flash('Invalid login credentials please check and retry', 'error')
        return redirect(url_for('login'))

    if not registered_user.check_password(password):
        flash('Password is invalid', 'error')
        return redirect(url_for('login'))

    login_user(registered_user, remember=remember_me)
    flash('Logged in successfully', 'success')
    return redirect(request.args.get('next') or url_for('index'))


@application.route('/logout')
@login_required
def logout():
    logout_user()
    flash('Successful logged out of clinic manager', category='success')
    return redirect(url_for('index'))


@login_manager.user_loader
def load_user(id):
    return User.query.get(int(id))


@application.before_request
def before_request():
    g.user = current_user


@application.route('/')
@login_required
def index():
    patient_count = Patient.query.count()
    appointment_count = Appointment.query.filter(Appointment.appt_date >= datetime.now().date(),
                                                 Appointment.appt_date <= (
                                                     datetime.now().date() + timedelta(days=1))).count()
    return render_template('index.html', appointments_today=appointment_count, patient_count=patient_count,
                           user_name=g.user.username, home_active='active')


@application.route('/treatments')
@login_required
def treatments():
    treatments_data = Treatment.query.all()
    return render_template('treatments.html', user_name=g.user.username, treatments_data=treatments_data,
                           treatments_active='active')


@application.route('/treatment', methods=['POST'])
@login_required
def treatment_data_handler():
    name = request.form['treatmentName']
    cost = request.form['cost']
    notes = request.form['notes']
    tid = request.form['tid']

    treatment = Treatment.query.filter_by(id=tid).first()
    if treatment is None:
        treatment = Treatment(name, cost, notes)
        db.session.add(treatment)
    else:
        # Update if not none
        treatment.name = name
        treatment.cost = cost
        treatment.notes = notes

    db.session.commit()
    flash('Treatment has been updated/added', 'success')
    return redirect(url_for('treatments'))


@application.route('/getTreatment/<tid>', methods=['GET'])
@login_required
def get_treatment(tid):
    treatment = Treatment.query.filter_by(id=tid).first()
    return json.dumps({'tid': treatment.id, 'name': treatment.name, 'cost': treatment.cost, 'notes': treatment.notes})


@application.route('/deleteTreatment/<tid>', methods=['GET'])
@login_required
def delete_treatment(tid):
    treatment = Treatment.query.filter_by(id=tid).first()
    flash('Treatment %s has been deleted' % treatment.name, 'success')
    db.session.delete(treatment)
    db.session.commit()
    return redirect(url_for('treatments'))


@application.route('/patients')
@login_required
def patients():
    return render_template('patients.html', user_name=g.user.username, patients_active='active')


@application.route('/patientsJson')
@login_required
def patients_json():
    json_data = []
    draw = int(request.args.get('draw'))
    start = int(request.args.get('start'))
    length = int(request.args.get('length'))
    keywords = request.args.get('search[value]')
    records_total = Patient.query.count()
    records_filtered = records_total

    if keywords:
        search_query = '%{0}%'.format(keywords)
        search_db_query = Patient.query.filter(or_(Patient.name.like(search_query),
                                                   Patient.mobile_number.like(search_query),
                                                   Patient.patient_number.like(search_query)))
        patients_data = search_db_query.all()
        records_filtered = search_db_query.count()
    else:
        patients_data = Patient.query.order_by(Patient.patient_number).all()

    for idx, patient in enumerate(patients_data):

        if idx < start:
            continue

        row = [
            patient.patient_number,
            '<a href="/patientProfile/%s">%s</a>' % (patient.id, patient.name),
            patient.mobile_number,
            # patient.contact_number,
            patient.email_address,
            # patient.gender,
            # patient.address,
            # patient.city,
            # patient.country,
            # patient.pincode,
            # patient.dob,
            # patient.blood_group,
            '<a href="#" data-toggle="modal" data-target="#patientModal" id="%s"'
            ' class="btn btn-primary btn-sm"><span class="glyphicon glyphicon-pencil"></span></a>' % patient.id,
            '<a href="/deletePatient/%s" class="btn btn-danger btn-sm">'
            '<span class="glyphicon glyphicon-trash"></span></a>' % patient.id,
            '<a href="/newAppointment/%s" class="btn btn-success btn-sm">'
            '<span class="glyphicon glyphicon-user"></span></a>' % patient.id
        ]
        json_data.append(row)

        if len(json_data) == length:
            break
    return jsonify({'data': json_data, 'recordsTotal': records_total,
                    'recordsFiltered': records_filtered, 'draw': draw})


@application.route('/patient', methods=['POST'])
# @login_required
def patient_data_handler():
    try:
        name = request.form.get('patientName') or None
        patient_number = request.form.get('patientNumber') or None
        mobile_number = request.form.get('mobileNumber') or None
        contact_number = request.form.get('contactNumber') or None
        email = request.form.get('email') or None
        gender = request.form.get('genderOptions') or None
        address = request.form.get('address') or None
        city = request.form.get('city') or None
        country = request.form.get('country') or None
        pincode = request.form.get('pincode') or None
        if request.form.get('age'):
            age = int(request.form.get('age')) or None
        dob = None

        if age:
            dob = datetime.now() - relativedelta(years=age)

        pid = request.form['pid'] or -1

        if patient_number is None or name is None:
            flash('Unable to add patient registration number and name is required', category='error')
            return redirect(url_for('patients'))

        patient = Patient.query.filter_by(id=pid).first()
        if patient is None:
            patient = Patient(name, patient_number, mobile_number, contact_number, email, gender,
                              address, city, country, pincode, dob, None)
            db.session.add(patient)
        else:
            # Update if not none
            patient.name = name
            patient.patient_number = patient_number
            patient.mobile_number = mobile_number
            patient.contact_number = contact_number
            patient.email = email
            patient.gender = gender
            patient.address = address
            patient.city = city
            patient.country = country
            patient.pincode = pincode
            patient.dob = dob
            patient.blood_group = None

        db.session.commit()
        flash('Patient has been updated/added', 'success')
    # except IntegrityError as e:
    #    flash('Duplicate entry for the patient', 'error')
    #    current_app.logger.error(e)
    except Exception as e:
        flash('An error occurred during the update/delete', 'error')
        current_app.logger.error(e)
    return redirect(url_for('patients'))


@application.route('/getPatient/<pid>', methods=['GET'])
@login_required
def get_patient(pid):
    patient = Patient.query.filter_by(id=pid).first()
    return json.dumps({'pid': patient.id,
                       'patient_number': patient.patient_number,
                       'name': patient.name,
                       'mobile_number': patient.mobile_number,
                       'contact_number': patient.contact_number,
                       'email_address': patient.email_address,
                       'gender': patient.gender,
                       'address': patient.address,
                       'city': patient.city,
                       'country': patient.country, 'pincode': patient.pincode, 'dob': patient.dob,
                       'blood_group': patient.blood_group})


@application.route('/deletePatient/<pid>', methods=['GET'])
@login_required
def delete_patient(pid):
    patient = Patient.query.filter_by(id=pid).first()
    flash('Patient %s has been deleted' % patient.name, 'success')
    db.session.delete(patient)
    db.session.commit()
    return redirect(url_for('patients'))


@application.route('/patientProfile/<pid>', methods=['GET'])
@login_required
def patient_profile(pid):
    patient = Patient.query.filter_by(id=pid).first()
    total_visits = Appointment.query.filter(Appointment.patient_id == pid).count()
    last_appointment = Appointment.query.filter(Appointment.patient_id == pid).order_by(Appointment.appt_date).first()
    last_visit_date = 'N/A'
    if last_appointment:
        last_visit_date = last_appointment.appt_date.strftime("%B %d, %Y %I:%M %p")
    return render_template('patient-profile.html', patient=patient, user_name=g.user.username,
                           total_visits=total_visits, last_visit_date=last_visit_date)


@application.route('/appointments')
@login_required
def appointments():
    return render_template('appointments.html', user_name=g.user.username, appointments_active='active')


@application.route('/appointmentsJson')
@login_required
def appointments_json():
    json_data = []
    draw = int(request.args.get('draw'))
    start = int(request.args.get('start'))
    length = int(request.args.get('length'))
    keywords = request.args.get('search[value]')
    records_total = Appointment.query.filter(Appointment.appt_date >= datetime.now().date()).count()
    records_filtered = records_total

    if keywords:
        search_query = '%{0}%'.format(keywords)
        search_db_query = Appointment.query.join(Patient).filter(or_(Patient.name.like(search_query),
                                                                     Patient.mobile_number.like(search_query),
                                                                     Patient.patient_number.like(search_query)))
        appointments_data = search_db_query.all()
        records_filtered = search_db_query.count()
    else:
        appointments_data = Appointment.query.filter(Appointment.appt_date >= datetime.now().date())

    for idx, appointment in enumerate(appointments_data):

        if idx < start:
            continue

        patient = Patient.query.filter_by(id=appointment.patient_id).first()

        if patient is None:
            continue

        row = [
            appointment.appt_date.strftime("%B %d, %Y %I:%M %p"),
            patient.patient_number,
            '<a href="/patientProfile/%s">%s</a>' % (patient.id, patient.name),
            patient.mobile_number,
            '<a href="/deleteAppointment/%s" class="btn btn-danger btn-sm">'
            '<span class="glyphicon glyphicon-trash"></span></a>' % appointment.id
        ]
        json_data.append(row)

        if len(json_data) == length:
            break
    return jsonify(
        {'data': json_data, 'recordsTotal': records_total, 'draw': draw, 'recordsFiltered': records_filtered})


@application.route('/appointmentsCalendarJson')
@login_required
def appointments_calendar_json():
    json_data = []
    start = datetime.strptime(request.args.get('start'), '%Y-%m-%d')
    end = datetime.strptime(request.args.get('end'), '%Y-%m-%d')

    appointments_data = Appointment.query.filter(Appointment.appt_date >= start, Appointment.appt_date <= end)

    for idx, appointment in enumerate(appointments_data):
        patient = Patient.query.filter_by(id=appointment.patient_id).first()

        if patient is None:
            continue

        event = {
            'id': appointment.id,
            'title': patient.name,
            'start': appointment.appt_date.isoformat(),
            'end': (appointment.appt_date + timedelta(minutes=15)).isoformat()
        }

        json_data.append(event)
    resp = Response(response=json.dumps(json_data), status=200, mimetype="application/json")
    return resp


@application.route('/newAppointment/<pid>', methods=['GET', 'POST'])
@login_required
def new_appointment(pid):
    if pid and request.method == 'GET':
        patient = Patient.query.filter_by(id=pid).first()
        treatments = Treatment.query.all()
        return render_template('new-appointment.html', user_name=g.user.username, patient_name=patient.name,
                               patient_mobile_number=patient.mobile_number, patient_id=patient.id,
                               treatments=treatments,
                               appointments_active='active')

    # POST Request Handling
    appointment_date = request.form.get('appointmentDateTime') or None
    patient_id = request.form.get('patientId')
    treatment_id = request.form.get('treatmentId') or None
    patient = Patient.query.filter_by(id=pid).first()
    if appointment_date and patient_id:
        appt_date = datetime.strptime(appointment_date, '%d/%m/%Y %H:%M')
        appointment = Appointment(patient_id, appt_date, treatment_id)
        db.session.add(appointment)
        db.session.commit()
        flash('Appointment created for patient %s for date %s' % (patient.name, appointment_date), category='success')
    else:
        flash('Appointment date is required for patient %s' % patient.name, category='error')

    return redirect(url_for('appointments'))


@application.route('/patientsAppointmentsJson/<pid>')
@login_required
def patient_appointments_json(pid):
    appointments_data = Appointment.query.filter(Appointment.patient_id == pid) \
        .order_by(desc(Appointment.appt_date)).all()
    json_data = []
    draw = int(request.args.get('draw'))
    start = int(request.args.get('start'))
    length = int(request.args.get('length'))
    records_total = Appointment.query.filter(Appointment.patient_id == pid).count()
    records_filtered = records_total

    for idx, appointment in enumerate(appointments_data):

        if idx < start:
            continue

        prescription = Prescription.query.filter(Prescription.id == appointment.prescription).first()
        prescription_button = '<a href="#" data-toggle="modal" data-target="#prescriptionModal" id="' \
                              + str(appointment.id) + '" ' \
                                                      'class="btn btn-primary btn-sm"> ' \
                                                      '<span class="glyphicon glyphicon-pencil"></span></a>',

        if prescription:
            view_button = ' <a href="/prescriptionView/%s" class="btn btn-info btn-sm">' \
                          '<span class="glyphicon glyphicon-link"></span></a>' % prescription.id
            prescription_button = '%s %s' % (prescription_button[0], view_button)

        treatment = Treatment.query.filter(Treatment.id == appointment.treatment).first()

        if treatment:
            treatment_name = treatment.name
        else:
            treatment_name = ''

        row = [
            appointment.appt_date.strftime("%B %d, %Y %I:%M %p"),
            treatment_name,
            prescription_button
        ]
        json_data.append(row)

        if len(json_data) == length:
            break
    return jsonify(
        {'data': json_data, 'recordsTotal': records_total, 'draw': draw, 'recordsFiltered': records_filtered})


@application.route('/deleteAppointment/<aid>', methods=['GET'])
@login_required
def delete_appointment(aid):
    appointment = Appointment.query.filter(Appointment.id == aid).first()
    flash('Appointment %s has been deleted' % appointment.id, 'success')
    db.session.delete(appointment)
    db.session.commit()
    return redirect(url_for('appointments'))


# @app.route('/prescriptionUpload/<pid>/<aid>', methods=['POST'])
# @login_required
# def prescription_upload(aid, pid):
#     appointment = Appointment.query.filter(Appointment.id == aid).first()
#     prescription_file = request.files['prescription_file']
#     if prescription_file and allowed_file(prescription_file.filename):
#
#         patient = Patient.query.filter(Patient.id == appointment.patient_id).first()
#
#         updated_filename = '{0}_{1}_{2}'.format(patient.patient_number,
#                                                 appointment.appt_date.strftime("%Y%m%d-%H%M%S"),
#                                                 prescription_file.filename)
#
#         filename = secure_filename(updated_filename)
#
#         path = app.config['UPLOAD_FOLDER'] + '/' + pid
#         if not os.path.exists(path):
#             os.makedirs(path)
#         prescription_file.save(os.path.join(path, filename))
#         prescription = Prescription(pid=pid, doc_url=filename)
#         db.session.add(prescription)
#         db.session.flush()
#         appointment.prescription = prescription.id
#         upload_prescription(appointment.patient_id, prescription.id, filename)
#         db.session.commit()
#     return redirect(url_for('patient_profile', pid=pid))


# @app.route('/prescriptionView/<prescription_id>')
# def prescription_view(prescription_id):
#     prescription = Prescription.query.filter(Prescription.id == prescription_id).first()
#
#     if prescription is None:
#         flash('Prescription not found please contact admin@centreforsleep.org', category='error')
#         return redirect(url_for('index'))
#
#     return redirect(get_expiring_prescription_url(prescription.patient_id, prescription.id, prescription.doc_url))

@application.route('/forgotpass')
def forgot_password():
    flash('For new password contact admin@centreforsleep.org', 'error')
    return redirect(url_for('login'))


@application.route('/<path:resource>')
@cache(max_age=86400)
def serve_static_resource(resource):
    return send_from_directory('static/', resource)


@application.route("/test")
def test():
    return "<strong>It's Alive!</strong>"


@application.template_filter('agefromdob')
def _jinja2_filter_agefromdob(date, fmt=None):
    return relativedelta(datetime.now(), date).years


@application.errorhandler(404)
def page_not_found(e):
    return render_template('404.html'), 404


@application.errorhandler(500)
def internal_error(e):
    return render_template('500.html'), 500


def allowed_file(filename):
    return '.' in filename and \
           filename.lower().rsplit('.', 1)[1] in ALLOWED_EXTENSIONS


@application.before_request
def before_request():
    if request.endpoint in FLASH_CLEAR_ENDPOINTS:
        session.pop('_flashes', None)


# All functions relating to Google Cloud Storage
# def _get_bucket():
#     try:
#         return gcloud_client.get_bucket(CLINIC_BUCKET_NAME)
#     except NotFound:
#         return gcloud_client.create_bucket(CLINIC_BUCKET_NAME)


class NoSuchPrescription(Exception):
    pass


class DuplicatePrescription(Exception):
    pass


#
# def upload_prescription(patient_id, prescription_id, filename, bucket=None):
#     if bucket is None:
#         bucket = _get_bucket()
#     basename = os.path.split(filename)[1]
#     blob = bucket.blob('%s/%s/%s' % (patient_id, prescription_id, basename))
#     if blob.exists(gcloud_client):
#         raise DuplicatePrescription(blob.name)
#     file_path = os.path.join(UPLOAD_FOLDER, str(patient_id), filename)
#     blob.upload_from_filename(file_path)
#     os.remove(file_path)
#
#
# def delete_prescription(patient_id, prescription_id, filename, bucket=None):
#     if bucket is None:
#         bucket = _get_bucket()
#     basename = os.path.split(filename)[1]
#     blob = bucket.blob('%s/%s/%s' % (patient_id, prescription_id, basename))
#     if not blob.exists(gcloud_client):
#         raise NoSuchPrescription(blob.name)
#     blob.delete()
#
#
# def get_expiring_prescription_url(patient_id, prescription_id, filename, bucket=None):
#     if bucket is None:
#         bucket = _get_bucket()
#     basename = os.path.split(filename)[1]
#     blob = bucket.blob('%s/%s/%s' % (patient_id, prescription_id, basename))
#     if not blob.exists(gcloud_client):
#         raise NoSuchPrescription(blob.name)
#
#     from oauth2client.service_account import ServiceAccountCredentials
#     scopes = ['https://www.googleapis.com/auth/drive']
#     credentials = ServiceAccountCredentials\
#         .from_json_keyfile_name(os.environ.get('GOOGLE_APPLICATION_CREDENTIALS'), scopes=scopes)
#
#     return blob.generate_signed_url(expiration=datetime.now() + timedelta(days=1),
#                                     credentials=credentials, client=gcloud_client)
#



def verify_env_variables():
    for var in ENV_VARS:
        if os.environ.has_key(var):
            continue
        else:
            raise RuntimeError('Environment Variable %s not defined' % var)


if __name__ == '__main__':
    verify_env_variables()
    application.run()
