Commands
========
oc login https://api.starter-us-east-1.openshift.com
oc project clinicmanager
oc status
oc logs -f bc/clinicmanager
oc get pods
